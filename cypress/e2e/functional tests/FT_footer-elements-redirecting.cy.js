const { footerElements } = require("../../pages/palm/footer-elements");

describe("Footer checks", () => {
    beforeEach(() => {
      cy.visit("/");
    });
  
    it("Check if footer links open in new tabs", {retries: 1}, () => {
      cy.get(footerElements.footerCopyright)
        .find("a")
        .then(($a) => {
          expect($a).to.have.attr("target", "_blank");
          expect($a).to.have.attr("href", "https://palmfdn.org/");
        });
  
      cy.get(footerElements.footerSocial)
        .find("a")
        .then(($a) => {
          expect($a.eq(0)).to.have.attr("target", "_blank");
          expect($a.eq(0)).to.have.attr("href", "https://t.me/PalmDAO_Community");
  
          expect($a.eq(1)).to.have.attr("target", "_blank");
          expect($a.eq(1)).to.have.attr("href", "https://twitter.com/palmfdn");
  
          expect($a.eq(2)).to.have.attr("target", "_blank");
          expect($a.eq(2)).to.have.attr(
            "href",
            "https://www.instagram.com/palmfdn"
          );
        });
    });
  });
  