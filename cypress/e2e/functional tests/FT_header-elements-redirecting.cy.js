const { headerElements } = require("../../pages/palm/header-elements");
const { reputationPageElements } = require("../../pages/palm/reputation-page");
const { mainPageElements } = require("../../pages/palm/main-page");

describe("Check header elements redirecting", () => {
  beforeEach(() => {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
    cy.visit("/");
  });

  it("Redirect to dashboard after log in and redirect to dashboard from logged in wallet button", () => {
    cy.get(headerElements.loginButton).click();
    cy.contains("MetaMask").click();
    cy.wait(1000);
    cy.url().should("include", "/reputation");
    //now return to main page and click again on the logged in wallet button
    cy.visit("/");
    cy.wait(500);
    cy.get(headerElements.loggedWalletButton).click();
    cy.wait(200);
    cy.url().should("include", "/reputation");
    
    cy.get(headerElements.loggedWalletButtonDIV).should((buttonElement) => {
      expect(buttonElement.eq(0).text()).to.match(/^0x/g);
      expect(buttonElement.eq(1)).to.have.descendants('img')
    });
  });

  it("log out check", () => {
    cy.get(headerElements.loginButton).click();
    cy.contains("MetaMask").click();
    cy.wait(1000);
    
    cy.get(headerElements.logOutButton).click()
    cy.get(headerElements.logOutButton).should('not.exist')

    cy.get(headerElements.loginButton)

    cy.get(headerElements.loginButton).should((buttonText) => {
      expect(buttonText.text()).to.have.string('connect');
    });
    
  })

  it("Redirect to governance page", () => {
    cy.get(headerElements.headerMenu.governance).should('have.css','font-weight','400').click();
    cy.wait(1000);
    cy.url().should("include", "/governance");
    cy.get(headerElements.headerMenuActive).should('have.css','font-weight','700')
  });

  it("Redirect to dashboard from menu", () => {
    cy.get(headerElements.headerMenu.dashboard).should('have.css','font-weight','400').click();
    cy.wait(1000);
    cy.url().should("include", "/reputation");
    cy.get(headerElements.headerMenuActive).should('have.css','font-weight','700')
  });

  it("Redirect to grant page", () => {
    cy.get(headerElements.headerMenu.grants).should('have.css','font-weight','400').click();
    cy.wait(1000);
    cy.url().should("include", "/grants");
    cy.get(headerElements.headerMenuActive).should('have.css','font-weight','700')
  });

  it("Redirect to mints page", () => {
    cy.get(headerElements.headerMenu.mints).should('have.css','font-weight','400').click();
    cy.wait(1000);
    cy.url().should("include", "/mints");
    cy.get(headerElements.headerMenuActive).should('have.css','font-weight','700')
  });

  it("Redirect to the main page from the logo", () => {
    cy.get(headerElements.headerDesktopLogo).first().click();
    cy.wait(1000);
    cy.url().should("eq", Cypress.config().baseUrl);
  });

  it("Check whether FAQ opens in new tab and if it has correct url", () => {
    cy.get(headerElements.headerMenu.faqPage)
      .should("be.visible")
      .then(($a) => {
        expect($a).to.have.attr("target", "_blank");
        expect($a).to.have.attr("href", "https://factorydao.notion.site/Palm-DAO-App-Guide-c904cbdcd4f244b09c7e634ae6cacff7")
      })
  });
});
