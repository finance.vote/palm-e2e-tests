const { mainPageElements } = require("../../pages/palm/main-page");

describe("Main page tests", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("Check if all main page links open in new tab", () => {
    cy.get(mainPageElements.newsText)
      .find("a")
      .each(($a) => {
        expect($a).to.have.attr("target", "_blank");
      });
    cy.get(mainPageElements.newsReadMore)
      .find("a")
      .each(($a) => {
        expect($a).to.have.attr("target", "_blank");
      });
  });
});

