const { mainPageElements } = require("../../pages/palm/main-page");
const { errorPageElements } = require("../../pages/palm/error-page");

describe("Main page tests", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("Banner checks", () => {
    cy.get(mainPageElements.bannerDesktop).click({ failOnStatusCode: false });
    cy.wait(1000);
    cy.url().should(
      "include",
      "/mints"
    );
    cy.get(errorPageElements.errorPage)
      .find("h1")
      .then(($h1) => {
        expect($h1).to.have.text("Oooops...");
      });
    cy.get(errorPageElements.errorPage)
      .find("p")
      .then(($p) => {
        expect($p.eq(0)).to.have.text("That page cannot be found.");
        expect($p.eq(1)).to.have.text("<- Got back to the Homepage");
      });
    cy.get(errorPageElements.errorPage)
      .find("p > a")
      .then(($a) => {
        expect($a).to.have.attr("href", "/");
      });
  });

  it("Home Page content check", () => {
    cy.get(mainPageElements.homePageTitle)
      .find("p")
      .then(($p) => {
        expect($p.eq(0)).to.have.text("Welcome");
        expect($p.eq(1)).to.have.text("to the Palm DAO portal");
        expect($p.eq(2)).to.have.text("connect your wallet to continue");
      });

    cy.get(mainPageElements.homePageWidgetContent)
      .eq(0)
      .find("div > div")
      .then(($div) => {
        expect($div.eq(0)).to.have.descendants("img");
        expect($div.eq(1)).to.have.text("Governance Program");
        expect($div.eq(2)).to.have.text(
          "A place where all are invited to join and collaborate as we build the tools to help communities, organizations, and maybe even governments, organize by leveraging blockchain technology."
        );
      });

    cy.get(mainPageElements.homePageWidgetContent)
      .eq(1)
      .find("div > div")
      .then(($div) => {
        expect($div.eq(0)).to.have.descendants("img");
        expect($div.eq(1)).to.have.text("Proof of Contribution");
        expect($div.eq(2)).to.have.text(
          "Introducing the Palm Foundation Proof of Contribution (“POC”). POCs are unique SBT (soul-bound token) collectables which can be earned by interacting with the Palm ecosystem."
        );
      });

    cy.get(mainPageElements.homePageWidgetContent)
      .eq(2)
      .find("div > div")
      .then(($div) => {
        expect($div.eq(0)).to.have.descendants("img");
        expect($div.eq(1)).to.have.text("How it works");
        expect($div.eq(2)).to.have.text(
          "Attend virtual / IRL events and/or Palm Academy courses, participate in votes and contribute to the ecosystem to claim your POCs. POCs cannot be transferred."
        );
        expect($div.eq(3)).to.have.text(
          "“The more POCs and $PALM you collect the more influence you will have in the Palm network”.  ~Maybe Yoda"
        );
      });

    cy.get(mainPageElements.homePageBigWidgetLeft)
      .find("div")
      .then(($div) => {
        expect($div.eq(0)).to.have.descendants("img");
        expect($div.eq(1)).to.have.text(
          "POC on the Road to Democratizing Art & Technology"
        );
        expect($div.eq(3)).to.have.text("The OG POC");
        expect($div.eq(4)).to.have.text(
          "Can only be claimed by addresses on the allowlist, a snapshot of Ethereum and Palm network activity or scavenger hunts and activities related to Palm network. The OG POCs will unlock the right to vote in specific Palm network governance votes."
        );
        expect($div.eq(5)).to.have.text(
          "A user not on the allowlist who collects 2 out of following 4 POC by the May 15th snapshot will be eligible to collect THE OG POCs."
        );
      });

    cy.get(mainPageElements.homePageBigWidgetCenter)
      .find("div")
      .then(($div) => {
        expect($div.eq(1)).to.have.text("February: Zona Maco POC");
        expect($div.eq(2)).to.have.text(
          "Attend the Zona Maco art fair in Mexico City (IRL) and a visit to the Palm Fdn booth."
        );
        expect($div.eq(4)).to.have.text("March: Growing Palm POC");
        expect($div.eq(5)).to.have.text(
          "Provide feedback on the Palm Governance DAO tool. Let's grow together!"
        );
      });

    cy.get(mainPageElements.homePageBigWidgetRight)
      .find("div")
      .then(($div) => {
        expect($div.eq(1)).to.have.text("April: Palm Bridge POC");
        expect($div.eq(2)).to.have.text(
          "Use the Palm Bridge to “bridge” an NFT from Ethereum to Palm or Palm to Ethereum."
        );
        expect($div.eq(4)).to.have.text("May: Palm Collector POC");
        expect($div.eq(5)).to.have.text(
          "Mint an NFT on the Palm network from one of our early contributors."
        );
      });

    cy.get(mainPageElements.newsHeader).should((newsHeader) => {
      expect(newsHeader).to.have.text("NEWS");
    });

    cy.get(mainPageElements.newsTextTitle).then((newsTextTitle) => {
      expect(newsTextTitle).to.have.text("PalmFdn @ ZsONAMACO");
    });

    cy.get(mainPageElements.newsText).then((newsText) => {
      expect(newsText).to.have.text(
        "Working with FactoryDAO and collaborating with ConsenSys, the Palm Foundation is driving the next generation of DAO governance technology by issuing a series of POCs (proofs of contribution) that can be collected by the Palm community to build their Palm Reputation. This will shape their access and influence in the DAO over the coming years and will provide Palm community members with direct influence over both the curatorial work of the Palm Foundation grant program and soon the wider protocol"
      );
    });
  });
});

