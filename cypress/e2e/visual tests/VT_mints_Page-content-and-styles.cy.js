const { mintsListPageElements } = require("../../pages/palm/mintsList-page");
import "cypress-real-events";

describe("Mints List page tests", () => {
  beforeEach(() => {
    cy.visit("/mints");
  });

  it("Content check", () => {
    cy.get(mintsListPageElements.mintsListTitle).then((govTitle) => {
      expect(govTitle).to.have.text("Mints");
    });
    cy.get(mintsListPageElements.mintsListDescription).then(
      (govDescription) => {
        expect(govDescription).to.contain(
          "Palm DAO aims to empower creators by providing a platform that allows for a fully decentralized and non-custodial flow from submission to mint of creators’ art. Enabling a framework for increased distribution and a new pathway for collectors to engage directly with artists."
        );
      }
    );
  });

  it("Collection visual checks", () => {
    cy.get(mintsListPageElements.singleCollection)
      .last()
      .then((collection) => {
        cy.get(mintsListPageElements.collection.collectionTag)
          .last()
          .should("have.css", "background-color", "rgb(0, 0, 0)")
          .and("have.css", "place-content", "center")
          .and("have.css", "color", "rgb(255, 255, 255)")
          .and("have.css", "text-transform", "uppercase")
          .and("have.css", "font-size", "26px")
          .and("have.css", "align-items", "center")
          .and("be.visible");
        cy.get(mintsListPageElements.collection.collectionVotingPeriod)
          .last()
          .should("contain", "Open")
          .and("have.css", "font-size", "14px")
          .and("have.css", "color", "rgb(132, 132, 132)")
        cy.get(mintsListPageElements.collection.collectionTitle)
          .last()
          .should("have.css", "font-weight", "700")
        cy.get(mintsListPageElements.collection.collectionCallToAction)
          .last()
          .should("have.css", "margin-bottom","0px")
        cy.get(mintsListPageElements.collection.collectionViewButton)
          .last()
          .should("have.css", "background-color", "rgb(255, 255, 255)")
          .and("have.css", "min-width", "248px")
          .and("have.css", "cursor", "pointer")
          .and("have.css", "height", "40px")
          .and("have.css", "border-top-color", "rgb(195, 194, 193)")
          .and("have.css", "border-top-style", "solid")
          .and("have.css", "border-top-width", "1px")
          .and("have.css", "color", "rgb(0, 0, 0)")
          .and("have.css", "font-size", "14px")
          .and("have.css", "text-transform", "uppercase")
        cy.get(mintsListPageElements.collection.collectionViewButton)
          .last()
          .realHover()
          .should("have.css", "background-color", "rgb(193, 254, 107)")
          .and('have.text','More')
      });
  });
});
