/*
TODO
niezalogowany:
kreska zamiast addresu
loader
palm og poc - 'not eligible'
updates section
brak grantsów
brak mintów
total POC - 0
$PALm - 0


*/

const { dashboardPageElements } = require("../../pages/palm/dashboard-page");

describe("Dashboard checks", () => {
  beforeEach(() => {
    cy.visit("/reputation");
  });

  it("Not logged in", () => {
    //wallet container
    cy.get(dashboardPageElements.wallet.loader).should('be.visible')
    cy.get(dashboardPageElements.wallet.address).should('have.text','-')
    cy.get(dashboardPageElements.wallet.totalPOC).eq(0).should('have.text','Proof of ContributionTotal POC collected')
    cy.get(dashboardPageElements.wallet.totalPOCAmount).eq(0).should('have.text','0')
    cy.get(dashboardPageElements.wallet.totalPOC).eq(1).should('have.text','$PALMget $PALM')
    cy.get(dashboardPageElements.wallet.claimPALM).should('have.text','get $PALM').and('have.attr','target','_blank')
    cy.get(dashboardPageElements.wallet.totalPOCAmount).eq(1).should('have.text','0.0')
    cy.get(dashboardPageElements.wallet.comingSoonSection).should('be.visible')

    //POC container
    cy.get(dashboardPageElements.palmOGPOCContainer.container)
  });
});
