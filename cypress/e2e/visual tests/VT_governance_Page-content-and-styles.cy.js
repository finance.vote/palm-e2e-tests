const { governancePageElements } = require("../../pages/palm/governance-page");
import "cypress-real-events";

describe("Governance page tests", () => {
  beforeEach(() => {
    cy.visit("/governance");
  });

  it("Content check", () => {
    cy.get(governancePageElements.governanceTitle).then((govTitle) => {
      expect(govTitle).to.have.text("Governance");
    });
    cy.get(governancePageElements.governanceDescription).then(
      (govDescription) => {
        expect(govDescription).to.contain(
          "the Palm Foundation is driving the next generation of DAO governance technology by issuing a series of POCs (proofs of contribution) that can be collected by the Palm community to build their Palm Reputation. This will shape their access and influence in the DAO over the coming years and will provide Palm community members with direct influence over both the curatorial work of the Palm Foundation grant program and soon the wider protocol governance of the Palm network."
        );
      }
    );
  });

  it("Proposal visual checks", () => {
    cy.get(governancePageElements.singleProposal)
      .first()
      .then((proposal) => {
        cy.get(governancePageElements.proposal.proposalTag)
          .last()
          .should("have.css", "background-color", "rgb(0, 0, 0)")
          .and("have.css", "place-content", "center")
          .and("have.css", "color", "rgb(255, 255, 255)")
          .and("have.css", "text-transform", "uppercase")
          .and("have.css", "font-size", "26px")
          .and("have.css", "align-items", "center")
          .and("be.visible");
        cy.get(governancePageElements.proposal.proposalVotingPeriod)
          .last()
          .should("contain", "Voting Period")
          .and("have.css", "font-size", "14px")
          .and("have.css", "color", "rgb(132, 132, 132)")
        cy.get(governancePageElements.proposal.proposalTitle)
          .last()
          .should("have.css", "font-weight", "700")
        cy.get(governancePageElements.proposal.proposalCallToAction)
          .last()
          .should("have.css", "margin-bottom","0px")
        cy.get(governancePageElements.proposal.proposalViewButton)
          .last()
          .should("have.css", "background-color", "rgb(255, 255, 255)")
          .and("have.css", "min-width", "248px")
          .and("have.css", "cursor", "pointer")
          .and("have.css", "height", "40px")
          .and("have.css", "border-top-color", "rgb(195, 194, 193)")
          .and("have.css", "border-top-style", "solid")
          .and("have.css", "border-top-width", "1px")
          .and("have.css", "color", "rgb(0, 0, 0)")
          .and("have.css", "font-size", "14px")
          .and("have.css", "text-transform", "uppercase")
        cy.get(governancePageElements.proposal.proposalViewButton)
          .last()
          .realHover()
          .should("have.css", "background-color", "rgb(240, 58, 71)")
          .and('have.text','View')
      });
  });
});
