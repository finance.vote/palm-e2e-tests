const {
  proposalPageElements,
  choicesSectionElements,
} = require("../../pages/palm/proposal-page");
const { governancePageElements } = require("../../pages/palm/governance-page");

describe("Proposal checks", () => {
  beforeEach(() => {
    cy.visit("/governance");
    cy.get(governancePageElements.proposal.proposalViewButton).last().click();
    cy.wait(5000);
  });

  it("Visual checks - proposal info, tab bar", () => {
    //proposal title
    cy.get(proposalPageElements.proposalTitle).then((title) => {
      expect(title.text()).to.have.length.at.least(1);
      expect(title).to.be.visible;
      expect(title).to.have.css("font-weight", "700");
      expect(title).to.have.css("color", "rgb(46, 43, 38)");
    });
    //proposal description
    cy.get(proposalPageElements.proposalDescription).then((description) => {
      expect(description.text()).to.have.length.at.least(1);
      expect(description).to.be.visible;
      expect(description).to.have.css("font-weight", "400");
      expect(description).to.have.css("color", "rgb(0, 0, 0)");
    });
    //proposal logo
    cy.get(proposalPageElements.proposalLogo).then((logo) => {
      //check if logo image exists
      if (logo.find("img").length > 0) {
        expect(logo.find("img")).to.have.attr("width", "272");
        expect(logo.find("img")).to.have.attr("height", "272");
      } else {
        cy.log("No proposal's logo found");
      }
    });
    //tabBar Active
    cy.get(proposalPageElements.chosenTab).then((chosenTab) => {
      expect(chosenTab).to.have.css(
        "box-shadow",
        "rgb(157, 240, 52) 0px -3px 0px 0px inset"
      );
      expect(chosenTab).to.have.css("color", "rgb(157, 240, 52)");
      expect(chosenTab).to.have.css("background-color", "rgb(46, 43, 38)");
    });
    //tabBar not Active
    cy.get(proposalPageElements.notChosenTab).then((notChosenTab) => {
      expect(notChosenTab).to.have.css(
        "box-shadow",
        "rgb(207, 212, 218) 0px -3px 0px 0px inset"
      );
    });
    //tabBar
    cy.get(proposalPageElements.tabBarItem).then((tabBarItem) => {
      expect(tabBarItem).to.have.css("padding", "16px 12px");
      expect(tabBarItem).to.have.css("font-weight", "400");
      expect(tabBarItem).to.have.css("cursor", "pointer");
      expect(tabBarItem).to.have.css("font-size", "16px");
    });
    //tabBar text
    cy.get(proposalPageElements.myVote).should("have.text", "My Vote");
    cy.get(proposalPageElements.voteResults).should(
      "have.text",
      "Vote Results"
    );
  });
  it("Visual checks - Allocation section 'My vote' tab", () => {
    //allocation section logged out
    cy.get(proposalPageElements.allocation.allocationConnectWallet)
      .should("have.text", "connect wallet")
      .and("have.css", "border-top-color", "rgb(0, 0, 0)")
      .and("have.css", "color", "rgb(0, 0, 0)")
      .and("have.css", "text-transform", "uppercase")
      .and("have.css", "font-weight", "600")
      .and("have.css", "background-color", "rgb(255, 255, 255)");
    cy.get(proposalPageElements.allocation.allocationText).should(
      "have.text",
      "Summary of your Vote"
    );
    cy.get(proposalPageElements.allocation.allocationChart)
      .scrollIntoView()
      .should("be.visible")
      .and("have.attr", "width", "464")
      .and("have.attr", "height", "464");
    cy.get(proposalPageElements.allocation.allocationVoteButton)
      .should("be.disabled")
      .and("have.text", "Submit Vote")
      .and("have.css", "cursor", "auto")
      .and("have.css", "background-color", "rgb(0, 0, 0)")
      .and("have.css", "color", "rgb(255, 255, 255)")
      .and("have.css", "text-transform", "uppercase")
      .and("have.css", "font-weight", "600");
  });
  it("Visual checks - Allocation section 'vote results' tab", () => {
    //allocation section logged out
    cy.contains("Vote Results").click();
    cy.get(proposalPageElements.allocation.allocationConnectWallet)
      .should("have.text", "connect wallet")
      .and("have.css", "border-top-color", "rgb(0, 0, 0)")
      .and("have.css", "color", "rgb(0, 0, 0)")
      .and("have.css", "text-transform", "uppercase")
      .and("have.css", "font-weight", "600")
      .and("have.css", "background-color", "rgb(255, 255, 255)");
    cy.get(proposalPageElements.allocation.allocationText).should(
      "have.text",
      "Summary of the Vote"
    );
    cy.get(proposalPageElements.allocation.allocationChart)
      .should("be.visible")
      .and("have.attr", "width", "464")
      .and("have.attr", "height", "464");
    cy.get(proposalPageElements.allocation.allocationVoteButton).should(
      "not.exist"
    );
    cy.get(proposalPageElements.allocation.allocationStats)
      .find("div")
      .then(($div) => {
        expect($div.eq(1)).to.have.text("No. of voters:");
        expect($div.eq(4)).to.have.text("Total votes:");
      });
  });

  it("Visual checks - choices: 'My Vote' tab ", () => {
    //choices section logged out
    //image
    cy.get(choicesSectionElements.choiceMedia)
      .should("be.visible")
      .and("have.css", "width", "324px")
      .and("have.css", "height", "324px")
      .first()
      .click();
    cy.get(choicesSectionElements.singleChoice)
      .then(($popup) => {
        expect(
          $popup.find(choicesSectionElements.choiceExpandViewCheck).length
        ).to.be.greaterThan(0);
      })
      .then(() => {
        cy.get(choicesSectionElements.choiceExpandViewCloseButton).click();
      });

    //title
    cy.get(choicesSectionElements.choiceTitle).then((choiceTitle) => {
      expect(choiceTitle).to.have.css("font-weight", "700");
      expect(choiceTitle).to.have.css("color", "rgb(0, 0, 0)");
      expect(choiceTitle.text()).to.have.length.at.least(1);
    });

    //description
    cy.get(choicesSectionElements.choiceDescription).then(
      (choiceDescription) => {
        expect(choiceDescription).to.have.css("font-weight", "400");
        expect(choiceDescription).to.have.css("color", "rgb(132, 132, 132)");
        expect(choiceDescription.text()).to.have.length.at.least(1);
        expect(choiceDescription).to.have.descendants("button");
      }
    );
    cy.get(choicesSectionElements.choiceDescription)
      .find("span")
      .should("contain", "...");

    //slider
    cy.get(choicesSectionElements.choiceSlider)
      .eq(0)
      .then((slider) => {
        expect(slider).to.have.css("margin-left", "20px");
        expect(slider).to.have.css("margin-right", "20px");
        expect(slider).to.have.css("text-indent", "-20px");
        expect(slider).to.have.css("z-index", "1");
        expect(slider).to.have.css("font-weight", "600");
        expect(slider).to.have.css("text-transform", "uppercase");
        expect(slider).to.have.css("color", "rgb(0, 0, 0)");
        expect(slider).to.have.css("margin-bottom", "30px");
      });
    cy.get(choicesSectionElements.choiceSliderText)
      .eq(0)
      .then((slider) => {
        expect(slider).to.have.text("slide to vote");
      });
    cy.get(choicesSectionElements.choiceSliderTooltip)
      .eq(0)
      .then((slider) => {
        expect(slider).to.have.text("Connect wallet to allocate votes");
      });
    cy.get(choicesSectionElements.sliderHandle)
      .eq(0)
      .should("have.css", "cursor", "not-allowed");

    //"more" button - POPUP
    cy.get(choicesSectionElements.choiceDescription)
      .find("button")
      .eq(0)
      .click();
    cy.get(choicesSectionElements.singleChoice).then(($popup) => {
      if (
        $popup.find(choicesSectionElements.choiceExpandViewCheck).length > 0
      ) {
        //popup image
        cy.get(choicesSectionElements.choiceExpandViewImage)
          .should("be.visible")
          .and("have.css", "max-width", "884px")
          .and("have.css", "max-height", "450px")
          .and(
            "have.css",
            "background-image",
            "linear-gradient(rgb(46, 43, 38), rgb(217, 217, 217))"
          )
          .and("have.css", "margin-bottom", "31px");

        //popup title
        cy.get(choicesSectionElements.choiceExpandViewTitle).each(
          (titleText) => {
            expect(titleText.text()).to.have.length.at.least(1);
            expect(titleText).to.be.visible;
          }
        );

        //popup description
        cy.get(choicesSectionElements.choiceExpandViewDescription).then(
          (descriptionText) => {
            expect(descriptionText).to.be.visible;
            expect(descriptionText).to.have.css("font-weight", "400");
            expect(descriptionText.text()).to.have.length.at.least(1);
          }
        );

        //popup close button
        cy.get(choicesSectionElements.choiceExpandViewCloseButton)
          .should("be.visible")
          .click()
          .then(() => {
            cy.get(choicesSectionElements.choiceExpandViewCheck).should(
              "not.exist"
            );
          });
      } else {
        cy.log("error: class should exists");
      }
    });
  });

  it("Visual checks - choices: 'Vote results' tab ", () => {
    cy.contains("Vote Results").click();
    //choices section logged out
    //rank badge
    cy.get(choicesSectionElements.rankingBadge).each((badge) => {
      expect(badge).to.have.css("font-weight", "700");
      expect(badge).to.have.css("width", "56px");
      expect(badge).to.have.css("height", "56px");
      expect(badge).to.have.css("color", "rgb(157, 240, 52)");
      expect(badge).to.have.css("background-color", "rgb(46, 43, 38)");
      expect(badge).to.have.css("z-index", "1");
    });
    //image
    cy.get(choicesSectionElements.choiceMedia)
      .eq(0)
      .should("be.visible")
      .and("have.css", "z-index", "0")
      .and("have.css", "height", "324px")
      .click();
    cy.get(choicesSectionElements.singleChoice)
      .then(($popup) => {
        expect(
          $popup.find(choicesSectionElements.choiceExpandViewCheck).length
        ).to.be.greaterThan(0);
      })
      .then(() => {
        cy.get(choicesSectionElements.choiceExpandViewCloseButton).click();
      });

    //"more" button - POPUP
    cy.get(choicesSectionElements.choiceDescription)
      .find("button")
      .eq(0)
      .click();
    cy.get(choicesSectionElements.singleChoice)
      .then(($popup) => {
        expect(
          $popup.find(choicesSectionElements.choiceExpandViewCheck).length
        ).to.be.greaterThan(0);
      })
      .then(() => {
        cy.get(choicesSectionElements.choiceExpandViewCloseButton).click();
      });

    //slider
    cy.get(choicesSectionElements.choiceSlider)
      .eq(0)
      .then((slider) => {
        expect(slider).to.contain("total votes: ");
      });
    cy.get(choicesSectionElements.sliderHandle).should("not.exist");
    cy.get(choicesSectionElements.sliderTrack).should(
      "have.css",
      "background-color",
      "rgb(133, 215, 30)"
    );
  });
});
