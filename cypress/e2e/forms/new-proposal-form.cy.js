import "../../support/commands_global.cy";
const { governancePageElements } = require("../../pages/palm/governance-page");
const { newProposalFormPage } = require("../../pages/palm/new-proposal-form");

describe("Create new grant and mock data", () => {
  beforeEach("", () => {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
    cy.visit("/governance");
    cy.logIn();
  });

  it("Create a new grant", () => {
    cy.get(governancePageElements.addProposalButton).click();

    //fill in task title, call to action, benefit and description
    cy.get(newProposalFormPage.addProposalTitle).type("Proposal test ");
    cy.get(newProposalFormPage.addProposalCallToAction).type("Proposal Call to action");
    cy.get(newProposalFormPage.addProposalBenefit).type("Proposal benefit ");
    cy.get(newProposalFormPage.addProposalDescription).type("test description");

    // set grant time
    // awaiting
    cy.setProposalStartAndEndDate(1, 5, 7, 0, 0, 0);

    //choose strategy
    cy.get(newProposalFormPage.formDropdown)
      .contains("Strategy")
      .then((dropdown) => {
        cy.wrap(dropdown).click();
        cy.get(".Dropdown-option").contains("NFT Palm testnet").click();
      });

    //choose tag
    cy.get(newProposalFormPage.formDropdown)
      .contains("Tag")
      .then((dropdown) => {
        cy.wrap(dropdown).click();
        cy.get(".Dropdown-option").contains("FOX COLLAB").click();
      });


    //add image
    cy.get(newProposalFormPage.addProposalImage).selectFile(
      "cypress/fixtures/images/grantImage.png"
    );

    //add choices
    cy.addNumberOfChoices(4)

    //publish
    cy.get("button")
      .contains("Publish")
      .then((publishButton) => {
        cy.wrap(publishButton).click();
        cy.wait(500);
        cy.confirmMetamaskSignatureRequest();
      });
  });
});
