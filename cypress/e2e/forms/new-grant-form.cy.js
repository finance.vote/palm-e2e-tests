import "../../support/commands_global.cy";
const { grantsPageElements } = require("../../pages/palm/grants-page");
const { newGrantFormPage } = require("../../pages/palm/new-grant-form");

describe("Create new grant and mock data", () => {
  beforeEach("", () => {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
    cy.visit("/grants");
    cy.logIn();
  });

  it("Create a new grant", () => {
    cy.get(grantsPageElements.addGrantButton).click();

    //fill in task title, call to action, benefit and description
    cy.get(newGrantFormPage.addGrantTitle).type("Grant test ");
    cy.get(newGrantFormPage.addGrantCallToAction).type("Grant Call to action");
    cy.get(newGrantFormPage.addGrantBenefit).type("Grant benefit ");
    cy.get(newGrantFormPage.addGrantDescription).type("test description");

    // set grant time
    // awaiting
    cy.setTaskStartEndAndRankingDate(1, 5, 7, 0, 0, 0);

    //choose tag
    cy.get(newGrantFormPage.tagDropdown)
      .contains("Tag")
      .then((dropdown) => {
        cy.wrap(dropdown).click();
        cy.get(".Dropdown-option").contains("FOX COLLAB").click();
      });

    //permissions
    const permissionContracts = [
      newGrantFormPage.addSubmissionPermission,
      newGrantFormPage.voteSubmissionPermission,
      newGrantFormPage.curatorialPermission,
    ];
    for (let permission of permissionContracts) {
      cy.get(permission)
        .click({ force: true })
        .then(() => {
          cy.get(".optionListContainer > .optionContainer")
            .eq(0)
            .then(() => {
              for (let i = 0; i < 3; i++) {
                cy.get("li.option.highlightOption.highlight").eq(0).click();
              }
            });
        });
    }

    //add logo and image
    cy.get(newGrantFormPage.addGrantLogo).selectFile(
      "cypress/fixtures/images/grantlogo.png"
    );
    cy.get(newGrantFormPage.addGrantImage).selectFile(
      "cypress/fixtures/images/grantImage.png"
    );

    //add Rules
    const rulesList = ["Rule1", "Rule2", "Rule3"];
    for (let i = 0; i < 3; i++) {
      cy.get(newGrantFormPage.addRuleButton).click();
    }
    cy.get(newGrantFormPage.addRuleInput).each((inputRule, index) => {
      cy.wrap(inputRule).type(rulesList[index]);
    });

    //publish
    cy.get("button")
      .contains("Publish")
      .then((publishButton) => {
        cy.wrap(publishButton).click();
        cy.wait(500);
        cy.confirmMetamaskSignatureRequest();
      });
  });
});
