const { headerElements } = require("../../pages/palm/header-elements");

describe("Tests with backend", () => {
  beforeEach("login to application", () => {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
    cy.intercept(
      "GET",
      "https://palm-gov-test.influencebackend.xyz/api/tasks/palm/QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe",
      { fixture: "awaitingGrant.json" }
    );

    cy.visit("/grants/QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe");

    cy.get(headerElements.loginButton).click();
    cy.contains("MetaMask")
      .click()

  });

  it("Add new submission should not be possible", () => {
    cy.get('[class^="grant_buttonContainer"] > a > button').should(
      "be.disabled"
    );
  });
});
