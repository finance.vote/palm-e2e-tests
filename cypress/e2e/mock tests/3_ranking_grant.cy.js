const { headerElements } = require("../../pages/palm/header-elements");

describe("Tests with backend", () => {
  beforeEach("login to application", () => {
    cy.intercept(
      "GET",
      "https://palm-gov-test.influencebackend.xyz/api/tasks/palm/QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe",
      { fixture: "rankingGrant.json" }
    );
    cy.intercept("GET","https://palm-gov-test.influencebackend.xyz/api/tasks/palm/testHash/check/0xd932de349a0e583b27cc98cc5025a4960c4da5e1", { fixture: "wallet.json" })

    cy.visit("/grants/QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe");

    cy.get(headerElements.loginButton).click();
    cy.contains("MetaMask")
      .click()

  });

  it("Voting on the submission", () => {
    cy.get('[class^="UpDownVote_score"]').eq(0).should("have.text", 0);

    cy.fixture("rankingGrant").then((file) => {
      file.submissions[0].upVotes = [
        "0xd932de349A0e583b27CC98cC5025a4960C4dA5E1",
      ];
      cy.intercept(
        "PATCH",
        "https://palm-gov-test.influencebackend.xyz/api/tasks/palm/vote/*",
        file
      );
    });
    cy.wait(300);
    cy.get('[class^="UpDownVote_voteUp"]').eq(0).click();
    cy.confirmMetamaskSignatureRequest();
    cy.get('[class^="UpDownVote_score"]').eq(0).should("have.text", 1);
  });

  it("Add new submission should not be possible", () => {
    cy.get('[class^="grant_buttonContainer"] > a > button')
      .first()
      .should("be.disabled");
  });
});
