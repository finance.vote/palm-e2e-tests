const { headerElements } = require("../../pages/palm/header-elements");

describe("Open grant: Tests with backend", () => {
  beforeEach("login to application", () => {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
    cy.intercept(
      "GET",
      "https://palm-gov-test.influencebackend.xyz/api/tasks/palm/QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe",
      { fixture: "openGrant.json" }
    );

    cy.visit("/grants/QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe");

    cy.get(headerElements.loginButton).click();
    cy.contains("MetaMask")
      .click()
  });

  it("Adding new submission should be possible", () => {
    cy.get('[class^="grant_buttonContainer"] > a > button').should(
      "not.be.disabled"
    );
  });

  it("Add new submission and vote on it", () => {
    cy.fixture("openGrant").then((file) => {
      file.submissions[0] = {
        _id: "64633489cdbab945da93af31",
        title: "First Submission",
        body: "Supports Unix timestamps in seconds, milliseconds, microseconds and nanoseconds.",
        userId: "0xd932de349A0e583b27CC98cC5025a4960C4dA5E1",
        voterAddress: "0xd932de349a0e583b27cc98cc5025a4960c4da5e1",
        timestamp: 1683292471842,
        submissionHash: "QmU9usVwPNvgNNJMtJAy84s9fxpf78JrCNHKk8pVruFGFt",
        upVotes: [],
        downVotes: [],
        richMedia: {
          _id: "64550140cdbab945da938648",
          type: "image",
          createdAt: "2023-05-05T13:14:40.566Z",
          updatedAt: "2023-05-05T13:14:40.610Z",
          __v: 0,
          link: "QmRZViVjNp1wsBdrCL5z1HHuMzbUj4ahr3dtqugA42C33h",
        },
        artist: {
          name: "John Doe",
          mediaLink: "wwwwww",
          bio: "Supports Unix timestamps in seconds, milliseconds, microseconds and nanoseconds.",
        },
        curatorialChoosen: false,
      };
      file.question.submissionsCount = 1;
      file.contributors["0xd932de349A0e583b27CC98cC5025a4960C4dA5E1"] = {
        walletAddress: "0xd932de349a0e583b27cc98cc5025a4960c4da5e1",
        submissionsCounter: 1,
      };
      cy.intercept(
        {
          method: "POST",
          url: "https://palm-gov-test.influencebackend.xyz/api/tasks/palm/QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe/*",
        },
        file
      );
    });

    cy.readFile("cypress/fixtures/openGrant.json").then((file) => {
      file.submissions[0] = {
        _id: "64633489cdbab945da93af31",
        title: "First Submission",
        body: "Supports Unix timestamps in seconds, milliseconds, microseconds and nanoseconds.",
        userId: "0xd932de349A0e583b27CC98cC5025a4960C4dA5E1",
        voterAddress: "0xd932de349a0e583b27cc98cc5025a4960c4da5e1",
        timestamp: 1683292471842,
        submissionHash: "QmU9usVwPNvgNNJMtJAy84s9fxpf78JrCNHKk8pVruFGFt",
        upVotes: [],
        downVotes: [],
        richMedia: {
          _id: "64550140cdbab945da938648",
          type: "image",
          createdAt: "2023-05-05T13:14:40.566Z",
          updatedAt: "2023-05-05T13:14:40.610Z",
          __v: 0,
          link: "QmRZViVjNp1wsBdrCL5z1HHuMzbUj4ahr3dtqugA42C33h",
        },
        artist: {
          name: "John Doe",
          mediaLink: "wwwwww",
          bio: "Supports Unix timestamps in seconds, milliseconds, microseconds and nanoseconds.",
        },
        curatorialChoosen: false,
      };
      file.question.submissionsCount = 1;
      file.contributors["0xd932de349A0e583b27CC98cC5025a4960C4dA5E1"] = {
        walletAddress: "0xd932de349a0e583b27cc98cc5025a4960c4da5e1",
        submissionsCounter: 1,
      };
      cy.intercept(
        {
          method: "GET",
          url: "https://palm-gov-test.influencebackend.xyz/api/tasks/palm/QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe",
        },
        file
      );
    });

    cy.contains("START APPLICATION").click();
    cy.get('[name="title"]').type("Name 4");
    cy.get("textarea").type("abcdef");
    cy.wait(200);
    cy.get('[type="submit"]').click();

    cy.get('[name="name"]').type("John Doe");
    cy.get('[name="artistBio"]').type("abcdef");
    cy.get('[name="mediaLinks"]').type("wwwwww");
    cy.wait(200);
    cy.get('[type="submit"]').click();

    cy.get('[class^="ArtUpload_dropArea"]').selectFile(
      "cypress/fixtures/images/1.png",
      { action: "drag-drop" }
    );
    cy.get('[class^="NextStepButton_nextBtn"]').click();

    cy.get('[class^="SubmitStep_checkboxContainer"] > [type="checkbox"]').check(
      { force: true }
    );
    cy.get('[class^="NextStepButton_nextBtn"]').click();
    cy.confirmMetamaskSignatureRequest();
    cy.wait(300);
    cy.contains("View Submissions").click();

    cy.get('[class^="UpDownVote_score"]').eq(0).should("have.text", 0);

    cy.fixture("openGrantWithSubmission").then((file) => {
      file.submissions[0].upVotes = [
        "0xd932de349A0e583b27CC98cC5025a4960C4dA5E1",
      ];
      cy.intercept(
        "PATCH",
        "https://palm-gov-test.influencebackend.xyz/api/tasks/palm/vote/*",
        file
      );
    });
    cy.wait(300);
    cy.get('[class^="UpDownVote_voteUp"]').eq(0).click();
    cy.confirmMetamaskSignatureRequest();
    cy.get('[class^="UpDownVote_score"]').eq(0).should("have.text", 1);
  });
});
