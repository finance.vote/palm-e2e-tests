const { headerElements } = require("../../pages/palm/header-elements");

describe("Tests with backend", () => {
  beforeEach("login to application", () => {
    cy.intercept(
      "GET",
      "https://palm-gov-test.influencebackend.xyz/api/tasks/palm/QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe",
      { fixture: "closedGrant.json" }
    );

    cy.visit("/grants/QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe");

    cy.get(headerElements.loginButton).click();
    cy.contains("MetaMask")
      .click()
  });

  
  it("Add vote and new submission should be impossible", () => {
    cy.get('[class^="grant_buttonContainer"] > a > button')
      .first()
      .should("be.disabled");

    cy.get('[class^="UpDownVote_voteUp"]').eq(0).should("be.disabled");
  });


  it('Curator vote', () => {
    cy.get('[class^="SingleSubmission_curatorialCheckbox"]').should('be.visible')
    cy.get('[class^="CuratorialHeader_curatorial"]').should('be.visible')
    cy.get('[class^="CuratorialHeader_header"] > button').should('be.visible').and('be.disabled')


    cy.get('[class^="SingleSubmission_curatorialCheckbox"]').then(checkBox => {
      cy.wrap(checkBox).click()
      cy.get('[class*="SingleSubmission_borderSelected"]').should('have.css', 'border', '5px solid rgb(193, 254, 107)')

      cy.get('[class^="SingleSubmission_mediaContainer"] > img').then(element => {
        cy.wrap(element[0]).its('0.data-loaded-src').as('elementSRC')
        cy.get('[class^="CuratorialHeader_choiceImageContainer"] > img').then(element => {
          cy.wrap(element[0]).invoke('attr','src').as('curatorialSRC')
        })
      })

      cy.get('@elementSRC').then(element1 => {
        cy.get('@curatorialSRC').should(element2 => {
          expect(element1).to.equal(element2)
        })
      }).then(() => {
        cy.intercept('POST','https://palm-gov-test.influencebackend.xyz/api/tasks/curatorial/QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe', {fixture: "curatorialvote.json"})
        cy.get('[class^="CuratorialHeader_header"] > button').should('not.be.disabled').click()
        cy.confirmMetamaskSignatureRequest().then(() => {
          
          cy.get('[class^="CuratorialHeader_header"] > button').should('not.exist')
          cy.get('[class^="CuratorialHeader_submitComplete"]').should('have.text','SELECTION COMPLETE')


        })
      })
    })
  })
});
