const { headerElements } = require("../pages/palm/header-elements");

//checking current and future time
Cypress.Commands.add("setDateTime", (day) => {
  let date = new Date();
  date.setDate(date.getDate() + day);
  let futureDay = date.getDate();
  let futureMonth = date.toLocaleString("en-US", { month: "long" });

  cy.get(".react-datepicker__current-month").then((dateAttribute) => {
    console.log(dateAttribute.text());
    if (!dateAttribute.text().includes(futureMonth)) {
      cy.get(
        '[class="react-datepicker__navigation react-datepicker__navigation--next"]'
      ).click();
      cy.setDateTime(day);
    } else {
      cy.get(
        ".react-datepicker__month .react-datepicker__day:not(.react-datepicker__day--disabled)"
      )
        .contains(futureDay)
        .click();
    }
  });
});

Cypress.Commands.add("setHourMinutes", (hour, minutes) => {
  let time0 = new Date();
  let time = new Date();
  // time.setHours(time.getHours() + hour)

  if (time.getHours() + hour > 24) {
    time.setHours(24);
  } else {
    time.setHours(time.getHours() + hour);
  }

  let currentHour = time.getHours();

  if (time.getMinutes() + minutes > 60) {
    let variousMinutes = time.getMinutes() + minutes;
    while (variousMinutes > 60) {
      variousMinutes -= 60;
      currentHour += 1;
    }
    time.setMinutes(time.getMinutes() + variousMinutes);
  } else {
    time.setMinutes(time.getMinutes() + minutes);
  }
  // time.setMinutes(time.getMinutes() + minutes)
  let currentMinutes = time.getMinutes();

  if (currentHour >= 24 || currentHour < time0.getHours()) {
    cy.get('[placeholder="hh"]').clear().type(24);
  } else {
    cy.get('[placeholder="hh"]').clear().type(currentHour);
  }
  cy.get('[placeholder="mm"]').clear().type(currentMinutes);
});

//click next or select button in calendar modal
Cypress.Commands.add("clickNext", (text) => {
  cy.get('*[class^="DateTimeSelector_footer"] > button')
    .contains(text)
    .then((nextButton) => {
      //or: .contains(/next|select/g)
      cy.wrap(nextButton).click();
    });
});

//pick a tag
Cypress.Commands.add("pickTag", (proposalTagName, proposalsOrSubmission) => {
  cy.proposalOrSubmissionCheck(proposalsOrSubmission).then(
    (proposalsOrSubmission_value) => {
      cy.get('*[class^="Section_filterBtn"]')
        .eq(proposalsOrSubmission_value)
        .click()
        .then(() => {
          cy.get('*[class^="FilterSpace_clearBtn"]').click();
        });

      cy.get('*[class^="Section_filterBtn"]')
        .eq(proposalsOrSubmission_value)
        .click()
        .then(() => {
          let currentTag = '[name="' + proposalTagName + '"]';
          cy.get(currentTag).click();
          cy.get('*[class^="FilterSpace_applyBtn"]').click();
        });
      cy.wait(1000);

      cy.get('*[class^="Section_container"]')
        .eq(proposalsOrSubmission_value)
        .then(($div) => {
          if ($div.text().includes("There aren't any")) {
            cy.log("No proposals or tasks available");
          } else {
            cy.get('*[class^="Section_container"]')
              .eq(proposalsOrSubmission_value)
              .find('*[class^="Sign_sign"]')
              .should("contain", proposalTagName);
          }
        });
    }
  );
});

Cypress.Commands.add("logIn", () => {
  cy.get(headerElements.loginButton).click();
  cy.contains("MetaMask").click();
});

Cypress.Commands.add(
  "setTaskStartEndAndRankingDate",
  (
    startDayNumber = 0,
    endDayNumber = 0,
    rankingEndDay = 1,
    hourStart = 0,
    minutesStart = 0,
    hourEnd = 0,
    minutesEnd = 5
  ) => {
    // setting task start time
    cy.get('#modal > button')
      .contains("Select start date")
      .then((startDate) => {
        cy.wrap(startDate).click();
        cy.setDateTime(startDayNumber);
      });

    cy.clickNext("Next");
    cy.setHourMinutes(hourStart, minutesStart);
    cy.clickNext("Select");

    // setting task end time
    cy.get('#modal > button')
      .contains("Select end date")
      .then((startDate) => {
        cy.wrap(startDate).click();
        cy.setDateTime(endDayNumber);
      });

    cy.clickNext("Next");
    cy.setHourMinutes(hourEnd, minutesEnd);
    cy.clickNext("Select");

    // setting ranking end time
    cy.get('#modal > button')
      .contains("Select ranking end date")
      .then((startDate) => {
        cy.wrap(startDate).click();
        cy.setDateTime(rankingEndDay);
      });

    cy.clickNext("Next");
    cy.setHourMinutes(hourEnd, minutesEnd);
    cy.clickNext("Select");
  }
);

//publish task
Cypress.Commands.add("publishTask", () => {
  cy.get('[class^="NewGrant_button"]').then((publishButton) => {
    cy.wrap(publishButton).click();
    cy.wait(500);
    cy.confirmMetamaskSignatureRequest();
  });
});

//extend list of choices up to predefined number
//fill in choices names and descriptions
Cypress.Commands.add('addNumberOfChoices', (numberOfChoices) => {
  cy.get('button').contains('Add choice').then(addChoiceButton => {
      for (let i = 1; numberOfChoices > i; i++) {
          cy.wrap(addChoiceButton).click()
      }
  })

  const animalIndex = new Set()                           //create a set to remove duplicated numbers of indexes
  while (animalIndex.size < numberOfChoices) {
      animalIndex.add(Math.floor(Math.random() * 31));
  }
  const animalIndexArray = Array.from(animalIndex)
  cy.get('*[class^="NewProposal_list"]').find('*[class^="NewProposal_choiceRow"]').each((listItem, index) => {

      const valueNameDescription = {
          "Tenrec": "A tenrec is any species of mammal within the afrotherian family Tenrecidae endemic to Madagascar. Tenrecs are widely diverse; as a result of convergent evolution some resemble hedgehogs, shrews, opossums, rats, or mice. They occupy aquatic, arboreal, terrestrial and fossorial environments.",
          "Dingo": "The dingo is an ancient lineage of dog found in Australia. Its taxonomic classification is debated; as indicated by the variety of scientific names presently applied in different publications, it is variously considered a form of domestic dog not warranting recognition as a subspecies; a subspecies of dog or wolf; or a full species in its own right.",
          "Orchid mantis": "Hymenopus coronatus is a mantis from the tropical forests of Southeast Asia. It is known by various common names including walking flower mantis and (pink) orchid mantis. It is one of several species known as flower mantises from their resemblance and behaviour. They are known to grab their prey with blinding speed.",
          "Starling": "The common starling or European starling, also known simply as the starling in Great Britain and Ireland, is a medium-sized passerine bird in the starling family, Sturnidae. It is about 20 cm long and has glossy black plumage with a metallic sheen, which is speckled with white at some times of year.",
          "Pugshire": "Pugshire is a small hybrid dog that is the result of breeding between the Pug and the Yorkshire Terrier. These dogs are sweet and loving, though they primarily enjoy being a doting lap dog. If you are looking for an affectionate dog for yourself, the Pugshire delivers.",
          "Asp": "Although this reptile has long been associated with the culture of ancient Egypt, historians believe the Egyptian symbolism referred to a horned viper or a species of cobra. Asps are not native to Egypt, and their triangular heads slightly resemble those of cobras. These snakes live in many forested areas of Europe, and their bites can be deadly.",
          "Aye Aye": "The Aye Aye is a species of Lemur that is found inhabiting the rainforests of Madagascar. The Aye Aye is not only the largest nocturnal primate in the world but is also one of the most unique and is in fact so strange in appearance, that it was thought to be a large species of Squirrel when it was first discovered.",
          "Axolotl": "Axolotls are often referred to as “Mexican walking fish,” but they are actually amphibians that prefer to live their entire lives underwater. These remarkable creatures can regenerate almost every part of their bodies if necessary, including their spines, internal organs and even some parts of their brains.",
          "Baiji": "The baiji is an animal that lives in the Yangtze River in China. There are believed to be very few baiji left in existence. Its official conservation status is Critically Endangered, but some scientists believe they are extinct.",
          "Baboon": "The Baboon is a medium to large sized species of Old World Monkey that is found in a variety of different habitats throughout Africa and in parts of Arabia. There are five different species of Baboon which are the Olive Baboon, the Guinea Baboon, the Chacma Baboon, the Yellow Baboon and the Hamadryas Baboon which differs most from the others wide its bright red face.",
          "Butterfly Fish": "The butterfly fish is a family of tropical marine animals that have adapted to life in and around coral reefs. The seemingly endless permutations of beautiful colors and patterns give each species an entirely distinctive appearance, much like the famous insect for which it’s named. In fact, the vivid appearance and rather docile personality have made them.",
          "Chimaera": "The chimaera is a unique, cartilaginous fish that lives in the depths of the ocean and is closely related to sharks, skates and rays. Not much is known about their lifestyle or reproductive habits, but scientists have identified over 50 chimaera fish species throughout the world’s oceans. Each family of chimaera fish has a distinctive, somewhat gruesome appearancen.",
          "Dwarf Hamster": "A dwarf hamster is a group of several subspecies, specifically dealing with hamsters that are smaller than four inches long. They often have a lifespan of two to three years, though some live less than this. They are incredibly smart, and some subspecies hibernate through the winter months underground. If you want to take a little extra time with this furry pet.",
          "Feist Dog": "The Feist is a small hunting dog that arose in the American south at some point in the 18th century. The name of the species probably derives from the obsolete word fice or fyst, which means to break wind. This dog has an uncertain origin, but there are a few different theories for how it was first bred.",
          "Eurasian Lynx": "Sporting distinctive ear tufts and long cheek hairs, this animal is an extraordinarily effective hunter thanks to its incredible power and speed. They need so much space to hunt and mate, however, that they rarely live around humans. The remote mountains and forests of Eurasia are their main home.",
          "Eskipoo": "The Eskipoo is a designer dog achieved by crossing an American Eskimo dog with a European poodle. This breed that weighs about 15 pounds stands about 11 inches tall. Breeders developed this designer breed as a happy-go-lucky family addition. Most have a double coat, which may make them ideal companions on cold winter days.",
          "Frogfish": "Frogfish are a group of fish that belong to the Antennariidae family. This makes them part of the anglerfish family, which uses camouflage and lures to draw in potential prey. These fish fascinate people with their behavior, as they usually lie in wait on the seafloor and move slowly until they don’t. When they snap up their prey, they can strike in mere milliseconds.",
          "Fruit Bat": "The fruit bat spends much of its time hanging upside down in trees or caves with other members of the same species. They are aptly named for their habit of feasting exclusively on fruits and plant parts. However, many species are currently endangered by hunting and habitat loss.",
          "Guppy": "Guppies are tiny tropical fish that live throughout the world. They are sometimes called rainbow fish or millionfish. There are 276 species of guppy that belong to the Poeciliidae family.",
          "Harris Hawk": "Also called the bay-winged hawk, the Harris hawk is one of only two species of raptors that go hunting in “packs.” This behavior is thought to be one of the adaptations that helps the bird to successfully capture prey. It also allows them to catch prey larger than a single bird could handle. ",
          "Housefly": "Up there with the cockroach, the housefly is one of the world’s most annoying insects. Nearly 90 percent of all species invading our habitats are houseflies. They survive on a diet of decomposing garbage and organic waste. Flies are one of the filthiest creatures on the planet, carrying millions of disease-infested microorganisms over their lifespan.",
          "Keeshond": "When pronouncing the word ‘keeshond,’ remember it sounds like you’re saying the words ‘case haunt.’ This medium-sized canine has a thick double coat and dark, searching eyes.",
          "Javanese Cat": "The intelligent and loving Javanese cat will want to be by your side. This breed loves to talk to you, and they will use different voices to indicate their needs. This breed was created in the late 1970s and early 1980s by crossing a Siamese with a Balinese cat.",
          "Loach": "The loach superfamily of fishes consists of over 1200 species, most of which are distributed in central and southern Asia. Some species are also found in Europe and Africa. They are known for their bottom-feeding and the barbels found close to their mouths. Loach fish prefer ponds and streams, either stagnant or fast-moving.",
          "Moorhen": "The common moorhen, also called the common gallinule, are birds that are found around the world, just about every place except for the polar regions and tropical jungles. These birds are black with distinctive yellow legs and a red beak with a shield that extends from their beaks up between their eyes and onto their foreheads.",
          "Muskox": "Musk oxen have physical adaptations that allow them to live in the harsh Arctic climate year-round. They are herbivores eating grass, roots, flowers, lichen, moss, and other plant life. Musk oxen live in groups called herds. These large mammals can live up to 20 years.",
          "Nyala": "Scientists believe that this shy member of the Bovidae family has been around for close to 6 million years, which makes it only second to the lesser kudu when it comes to species longevity. Because of this, some believe it’s high time to put the nyala in its own genus, but for now, it remains a member of Tragelaphus",
          "Pheasant": "Pheasants are gorgeous game birds that feature beautiful plumage and long, powerful legs. There are an estimated 49 pheasant species, but the common pheasant, the Golden Pheasant, the Reeves’s Pheasant, and the silver pheasant are some of the most well-known types.",
          "Puffin": "The Puffin is a small species of seabird that is closely related to other auks such as guillemots. There are four different species of Puffin that are found inhabiting the colder conditions of the northern Atlantic which are the Atlantic Puffin, the Tufted Puffin, the Horned Puffin and the Rhinoceros Auklet that despite its name and differing appearance remains one.",
          "Quail": "Quail are plump, short-necked game birds whose natural habitat includes large areas of North America, Europe, Asia, and northern Africa. They also inhabit South America and Australia to a lesser extent. Some species have been domesticated and raised on farms for their meat and eggs, while populations in certain locations often hunt wild quail.",
          "Sturgeon": "Native to temperate waters of the Northern Hemisphere, sturgeons are among the largest fish and are found in the greatest abundance in the rivers of Ukraine and southern Russia and freshwater areas of North America."
      };

      const choicesImages = [
      "cypress/fixtures/images/choice_1.gif",
      "cypress/fixtures/images/choice_2.mp4",
      "cypress/fixtures/images/choice_3.jpg",
      "cypress/fixtures/images/choice_4.jpg"
      ]

      const currentName = Object.keys(valueNameDescription)[animalIndexArray[index]]
      const currentDescription = Object.values(valueNameDescription)[animalIndexArray[index]]
      cy.get('[placeholder="Name"]').eq(index).wait(250).type(currentName)
      cy.get('[data-role="descriptioninput"]').eq(index).wait(250).type(currentDescription)
      cy.get('[name="image"]').eq(index).selectFile(choicesImages[index])

  })
})

//setting proposal start and end dates
Cypress.Commands.add('setProposalStartAndEndDate', (startDayNumber = 0, endDayNumber = 0, hourStart = 0, minutesStart = 0, hourEnd = 0, minutesEnd = 5) => {
  // setting proposal start time
  cy.get('[data-test="proposalStartDateSelector"] > button').then(startDate => {
      cy.wrap(startDate).click()
      cy.setDateTime(startDayNumber)
  })

  cy.clickNext("Next")
  cy.setHourMinutes(hourStart, minutesStart)
  cy.clickNext("Select")

  // setting proposal end time
  cy.get('[data-test="proposalEndDateSelector"] > button').then(startDate => {
      cy.wrap(startDate).click()
      cy.setDateTime(endDayNumber)
  })

  cy.clickNext("Next")
  cy.setHourMinutes(hourEnd, minutesEnd)
  cy.clickNext("Select")

})