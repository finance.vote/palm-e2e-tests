const grantsTitle = '[class^="HeaderWithButton_header"] > h1';
const grantsDescription = '[class^="Grants_content"]';

const grantsList = '[class^="Grants_grantsList"]';
const singleGrant = `${grantsList} [class^="Section_container"]`;
const grant = {
  grant: singleGrant,
  grantTag: `${singleGrant} [class^="Section_tag"]`,
  grantVotingPeriod: `${singleGrant} > div [class^="Section_votingPeriod"]`,
  grantTitle: `${singleGrant} > div > h1`,
  grantCallToAction: `${singleGrant} [class^="Section_innerCenter"]`,
  grantViewButton: `${singleGrant} [class^="Section_innerRight"] > button`,
};
const addGrantButton = '[class^="HeaderWithButton_linkButton"]';

module.exports.grantsPageElements = {
  grantsTitle,
  grantsDescription,
  grantsList,
  singleGrant,
  grant,
  addGrantButton,
};
