const proposalInfo = '[class^="SingleProposal_info"]';
const proposalTitle = `${proposalInfo} [class^="SingleProposal_header"]`;
const proposalDescription = `${proposalInfo} [class^="SingleProposal_description"]`;
const proposalLogo = `[class^="SingleProposal_logo"]`

const proposalTabBar = `[class^="TabBar_container"]`
const myVote = `${proposalTabBar} > div:nth-child(1)`
const voteResults = `${proposalTabBar} > div:nth-child(2)`
const chosenTab = '[class*="TabBar_selected"]'
const notChosenTab = '[class*="TabBar_normal"]'
const tabBarItem = '[class*="TabBar_item"]'

const allocationSection = '[class^="SingleProposal_allocation"]'
const allocationBar = `${allocationSection} [class^="Allocation_container"] [class^="Allocation_progressbar"]`
const allocation = {
    allocationConnectWallet : `${allocationSection} [class^="SingleProposal_connectWalletContainer"] > button`,
    allocationBarLabel : `${allocationBar} [class^="Allocation_label"]`,
    allocationBarProgress : `${allocationBar} [class^="Progress_progress"]`,
    allocationText : `${allocationSection} [class^="SingleProposal_allocationText"]`,
    allocationChart : `${allocationSection} [class^="Chart_container"] > canvas`,
    allocationVoteButton : `${allocationSection} [class^="SubmitVoteButton_voteButton"]`,
    allocationStats: `${allocationSection} [class^="VoteStats_container"]`
}

module.exports.proposalPageElements = {
    proposalTitle,
    proposalDescription,
    proposalLogo,
    myVote,
    voteResults,
    chosenTab,
    notChosenTab,
    tabBarItem,
    allocation,
}

const choicesSection = '[class^="SingleProposal_choices"]'
const singleChoice =`${choicesSection} [class^="SingleProposal_choice"] [class^="SingleVote_container"]`
const rankingBadge = `${singleChoice} [class^="SingleVote_rankingBadge"]`
const choiceMedia = `${singleChoice} [class^="SingleVote_mediaContainer"]`
const choiceTitle = `${singleChoice} [class^="SingleVote_title"]`
const choiceDescription = `${singleChoice} [class^="SingleVote_description"] > div`
const choiceSlider = `${singleChoice} [class^="SingleVote_slideComponent"]`
const choiceSliderText = `${singleChoice} [class^="SingleVote_slideComponent"] > span`
const choiceSliderTooltip = `${singleChoice} [class^="SingleVote_slideComponent"] > [class^="VoteSlider_container"] > div > [class^="VoteSlider_tooltiptext"]`
const sliderHandle = '[class="rc-slider-handle"]'
const sliderTrack = '[class="rc-slider-track"]'

const choiceExpandViewCheck = `[class^="DialogWhite_dialog"]`
const choiceExpandView = `${singleChoice} [class^="DialogWhite_dialog"]`
const choiceExpandViewCloseButton = `${choiceExpandView} [class^="DialogWhite_close"]`
const choiceExpandViewImage = `${choiceExpandView} [class^="SingleVote_modalImageContainer"]`
const choiceExpandViewTitle =`${choiceExpandView} [class^="SingleVote_modalTitle"]`
const choiceExpandViewDescription = `${choiceExpandView} [class^="Markdown_markdown"]`

module.exports.choicesSectionElements = {
    singleChoice,
    rankingBadge,
    choiceMedia,
    choiceTitle,
    choiceDescription,
    choiceSlider,
    choiceSliderText,
    choiceSliderTooltip,
    sliderHandle,
    sliderTrack,
    choiceExpandViewCheck,
    choiceExpandView,
    choiceExpandViewCloseButton,
    choiceExpandViewImage,
    choiceExpandViewTitle,
    choiceExpandViewDescription,
}