const footer = `[class^="Footer_container"]`;
const footerCopyright = `${footer} [class^="Footer_copyright"]`;
const footerSocial = `${footer} [class^="Footer_social"]`;

module.exports.footerElements = {
    footer,
    footerCopyright,
    footerSocial
}