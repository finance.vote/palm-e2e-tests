const mainPage = '*[class^="HomePage_container"]';

const bannerDesktop = `${mainPage} > img`;
const bannerMobile = `[class^="ConnectWalletBanner_mobileBannerContainer"] > img`;

const homePageTitle = `${mainPage} [class^="HomePage_title"]`;
const homePageLine = `${mainPage} [class^="HomePage_line"]`;
const homePageWidgets = `${mainPage} [class^="HomePage_widgets"]`;
const homePageWidgetContent = `${homePageWidgets} [class^="HomePage_simpleWidget"]`;
const homePageBigWidget = `${mainPage} [class^="HomePage_bigWidget"] [class^="BigWidget_container"]`;
const homePageBigWidgetLeft = `${homePageBigWidget} [class^="BigWidget_leftSide"]`;
const homePageBigWidgetCenter = `${homePageBigWidget} [class^="BigWidget_center"]`;
const homePageBigWidgetRight = `${homePageBigWidget} [class^="BigWidget_rightSide"]`;


const newsWidget = `[class^="News_description"]`;
const newsHeader = `[class^="News_header"]`;
const newsTextTitle = `${newsWidget} [class^="News_title"]`;
const newsText = `${newsWidget} [class^="News_mainText"]`;
const newsReadMore = `${newsWidget} [class^="News_readMore"]`;


module.exports.mainPageElements = {
    mainPage,
    bannerDesktop,
    bannerMobile,
    homePageTitle,
    homePageLine,
    homePageWidgets,
    homePageWidgetContent,
    homePageBigWidget,
    homePageBigWidgetLeft,
    homePageBigWidgetCenter,
    homePageBigWidgetRight,
    newsWidget,
    newsHeader,
    newsTextTitle,
    newsText,
    newsReadMore,
}