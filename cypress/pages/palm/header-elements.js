const headerContainer = '*[class^="Header_container"]';
const headerDesktopLogo = `[class^="Header_logo"] > img`;
const headerMobileLogo = `[class^="Header_mobileLogo"] > img`;
const headerMenu = {
  governance: `[class^="NavItem_item"]:nth-child(1)`,
  dashboard: `[class^="NavItem_item"]:nth-child(2)`,
  grants: `[class^="NavItem_item"]:nth-child(3)`,
  mints: `[class^="NavItem_item"]:nth-child(4)`,
  faqPage: `[class^="NavItem_item"]:nth-child(5)`,
};
const headerMenuActive = '[class*="NavItem_active"]'


const headerLogIn = '*[class^="Header_right"]';
const loginButton = `${headerLogIn} [class^="WalletButton_connectButton"]`;
const loggedWalletButton = `[class^="WalletButton_walletAddressContainer"]`;
const loggedWalletButtonDIV = `[class^="WalletButton_walletAddressContainer"] > div`;
const logOutButton = '[class^="WalletButton_logoutButton"]';



module.exports.headerElements = {
  headerContainer,
  headerDesktopLogo,
  headerMobileLogo,
  headerMenu,
  headerMenuActive,
  loginButton,
  loggedWalletButton,
  loggedWalletButtonDIV,
  logOutButton
};


