const governanceTitle = '[class^="HeaderWithButton_header"] > h1';
const governanceDescription = '[class^="ProposalsList_content"]';

const proposalsList = '[class^="ProposalList_proposalList"]';
const singleProposal = `${proposalsList} [class^="Section_container"]`
const proposal = {
    proposal: singleProposal,
    proposalTag: `${singleProposal} [class^="Section_tag"]`,
    proposalVotingPeriod: `${singleProposal} > div [class^="Section_votingPeriod"]`,
    proposalTitle: `${singleProposal} > div > h1`,
    proposalCallToAction: `${singleProposal} [class^="Section_innerCenter"]`,
    proposalViewButton: `${singleProposal} [class^="Section_innerRight"] > button`,
}

const addProposalButton = '[class^="HeaderWithButton_linkButton"]';


module.exports.governancePageElements = {
    governanceTitle,
    governanceDescription,
    proposalsList,
    singleProposal,
    proposal,
    addProposalButton
}