const formDropdown =
  '[class^="NewProposal_info"] > .Dropdown-root > .Dropdown-control';
const addNewTag = '[class^="NewProposal_newTagButton"]';

const addProposalImage = '[name="proposalImage"]';

const addProposalTitle = '[placeholder="Question"]';
const addProposalCallToAction = '[placeholder="Call to action"]';
const addProposalBenefit = '[placeholder="benefit"]';
const addProposalDescription = '[placeholder="What is your proposal?"]';


module.exports.newProposalFormPage = {
  formDropdown,
  addNewTag,
  addProposalImage,

  addProposalTitle,
  addProposalCallToAction,
  addProposalBenefit,
  addProposalDescription,
};
