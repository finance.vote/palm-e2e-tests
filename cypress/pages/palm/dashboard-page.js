const walletContainer = '[class^="Wallet_container"]'
const walletContainer_Container = `${walletContainer} [class^="Wallet_addressAndImageContainer"]`
const infoRow = `[class^="Wallet_infoRow"]`

const wallet = {
    wallet: walletContainer_Container,
    loader:`${walletContainer_Container} [class^="ReputationContainer_mobileImage"]`,
    nftAvatar: `${walletContainer_Container} [class^="NftImage_image___tNna"]`,
    defaultAvatar: `${walletContainer_Container} [class^="Wallet_blockies"]`,
    address:`${walletContainer_Container} [class^="Wallet_walletAddress"]`,
    totalPOC:`${infoRow} > :nth-child(1)`,
    totalPOCAmount:`${infoRow} [class^="Wallet_amount"]`,
    totalPALM:`${infoRow} > div:nth-child(3)`,
    claimPALM:`${infoRow} [class^="Wallet_claimPalm"]`,
    totalPALMamount:`${infoRow} [class^="Wallet_amount"]:nth-child(2)`,
    comingSoonSection: '[class^="Wallet_comingSoonContainer"]'
}

const listContainer = '[class^="Reputation_listContainer"]'
const reputationContainer = `${listContainer} [class^="ReputationContainer_container"]`
const palmOGPOC = `${reputationContainer} [class^="Votes_pocContainer"]`
const palmOGPOCContainer = {
    container: palmOGPOC,
    title: `${palmOGPOC} [class^="Votes_pocLeft"] > div`,
    button: `${palmOGPOC} [class^="Votes_pocLeft"] > a`,
    logo: `${palmOGPOC} [class^="Votes_pocRight"] > img`
}

const updateSectionTitle = `${reputationContainer} [class^="ReputationContainer_h2"]:nth-child(1)`
const updateSectionDescription = {
    line1: `${reputationContainer} > p:nth-child(1)`,
    line2: `${reputationContainer} > p:nth-child(2)`,
    line3: `${reputationContainer} > p:nth-child(3)`,
}

const governanceTitle = `${reputationContainer} [class^="ReputationContainer_h2"]:nth-child(2)`

module.exports.dashboardPageElements = {
    walletContainer,
    wallet,
    listContainer,
    reputationContainer,
    palmOGPOC,
    palmOGPOCContainer,
    updateSectionTitle,
    updateSectionDescription,
    governanceTitle
}