const tagDropdown = '[class^="NewGrant_info"] > .Dropdown-root > .Dropdown-control';
const addNewTag = '[class^="NewGrant_newTagButton"]'
const setSubmissionMaxLength = '[name="maxLength"]'
const addGrantImage = '[name="grantImage"]'
const addGrantLogo = '[name="grantLogo"]'
const setSubmissionPerAddress = '[name="submissionsPerAddress"]'

const addGrantTitle = '[placeholder="Title"]';
const addGrantCallToAction = '[placeholder="Call to action"]';
const addGrantBenefit = '[placeholder="Benefit"]';
const addGrantDescription = '[placeholder="Add a description"]';
const addRuleButton = '[data-test="simple-input-add-new-element"]'
const addRuleInput = '[data-test="rule-input-element"]'

const addSubmissionPermission = '[name="addPermContracts_input"]'
const voteSubmissionPermission = '[name="votePermContracts_input"]'
const curatorialPermission = '[name="curatorialPermContracts_input"]'

module.exports.newGrantFormPage = {
  tagDropdown,
  addNewTag,
  setSubmissionMaxLength,
  addGrantImage,
  addGrantLogo,
  setSubmissionPerAddress,

  addGrantTitle,
  addGrantCallToAction,
  addGrantBenefit,
  addGrantDescription,
  addRuleButton,
  addRuleInput,

  addSubmissionPermission,
  voteSubmissionPermission,
  curatorialPermission,

};
