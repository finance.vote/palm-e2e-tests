const mintsListTitle = '[class^="Mints_title"]';
const mintsListDescription = '[class^="Mints_content"]';

const collectionList = '[class^="Mints_mintsList"]';
const singleCollection = `${collectionList} [class^="Section_container"]`
const collection = {
    collection: singleCollection,
    collectionTag: `${singleCollection} [class^="Section_tag"]`,
    collectionVotingPeriod: `${singleCollection} > div [class^="Section_votingPeriod"]`,
    collectionTitle: `${singleCollection} > div > h1`,
    collectionCallToAction: `${singleCollection} [class^="Section_innerCenter"]`,
    collectionViewButton: `${singleCollection} [class^="Section_innerRight"] > button`,
}

module.exports.mintsListPageElements = {
    mintsListTitle,
    mintsListDescription,
    collectionList,
    singleCollection,
    collection,

}