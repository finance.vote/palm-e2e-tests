const unlockPage = '.unlock-page';
const passwordInput = `${unlockPage} #password-label`;
const unlockButton = `${unlockPage} [data-testid="unlock-submit"]`;

module.exports.unlockPageElements = {
  unlockPage,
  passwordInput,
  unlockButton,
};
