const app = '#app-content';
// const app = '#app-content .app';
// const welcomePage = '.welcome-page';
const welcomePage = '.onboarding-welcome';
const criticalError = `${app} .critical-error`
// const confirmButton = `${welcomePage} .first-time-flow__button`;
module.exports.welcomePageElements = {
  app,
  welcomePage,
  // confirmButton,
  criticalError,
};

const firstTimeFlowPage = '.onboarding-welcome__buttons';
const importWalletButton = `${firstTimeFlowPage} [data-testid="onboarding-import-wallet"]`;
const createWalletButton = `${firstTimeFlowPage} [data-testid="onboarding-create-wallet"]`;
module.exports.firstTimeFlowPageElements = {
  importWalletButton,
  createWalletButton,
};

const metametricsPage = '.onboarding-metametrics__buttons';
const optOutAnalyticsButton = `${metametricsPage} [data-testid="metametrics-no-thanks"]`;
module.exports.metametricsPageElements = {
  metametricsPage,
  optOutAnalyticsButton,
};

const seedInputForm = '.import-srp';
// ttttttttooo dooo
// const secretWordsInput = `.import-srp .import-srp__srp .import-srp__srp-word`;
// `.import-srp .import-srp__srp .import-srp__srp-word:nth-child(1) [id="import-srp__srp-word-0"]`
const secretWordsInputArray = ['[id="import-srp__srp-word-0"]', '[id="import-srp__srp-word-1"]', '[id="import-srp__srp-word-2"]', '[id="import-srp__srp-word-3"]', '[id="import-srp__srp-word-4"]', '[id="import-srp__srp-word-5"]', '[id="import-srp__srp-word-6"]', '[id="import-srp__srp-word-7"]', '[id="import-srp__srp-word-8"]', '[id="import-srp__srp-word-9"]', '[id="import-srp__srp-word-10"]', '[id="import-srp__srp-word-11"]']
const importButton = `${seedInputForm} [data-testid="import-srp-confirm"]`;
const onBoardingFlow = '.onboarding-flow';
const passwordForm = '.create-password__form';
const passwordInput = `${passwordForm} [data-testid="create-password-new"]`;
const confirmPasswordInput = `${passwordForm} [data-testid="create-password-confirm"]`;
const termsCheckbox = `${passwordForm} [data-testid="create-password-terms"]`;
const importMyWalletButton = `${passwordForm} [data-testid="create-password-import"]`;

// const newPasswordInput = `${firstTimeFlowFormPage} #create-password`;
// const newSignupCheckbox = `${firstTimeFlowFormPage} .first-time-flow__checkbox`;

module.exports.firstTimeFlowFormPageElements = {
  seedInputForm,
  // firstTimeFlowFormPage,
  // secretWordsInput,
  secretWordsInputArray,
  importButton,
  onBoardingFlow,
  passwordForm,
  passwordInput,
  confirmPasswordInput,
  termsCheckbox,
  importMyWalletButton,
  // newPasswordInput,
  // newSignupCheckbox,
};

const secureYourWalletPage = '.seed-phrase-intro';
const nextButton = `${secureYourWalletPage} button`;
module.exports.secureYourWalletPageElements = {
  secureYourWalletPage,
  nextButton,
};

// const endOfFlowPage = '.end-of-flow';
const allDoneButton = '[data-testid="onboarding-complete-done"]';
module.exports.endOfFlowPageElements = {
  // endOfFlowPage,
  allDoneButton,
};

const revealSeedPage = '.reveal-seed-phrase';
const remindLaterButton = `${revealSeedPage} .first-time-flow__button`;
module.exports.revealSeedPageElements = {
  revealSeedPage,
  remindLaterButton,
};
