const path = require("path");
const fs = require("fs");

const mockGrants = {
  awaitingGrant: {
    question: {
      texts: {
        callToAction:
          "A small river named Duden flows by their place and supplies it with the necessary regelialia",
        benefit: "Little Blind Text didn’t listen",
      },
      _id: "testID",
      questionHash: "testHash",
      title: "The Big Oxmox advised",
      body: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way. On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.\n\n',
      startDate: Math.round(Date.now()) + 3 * 86400,
      endDate: Math.round(Date.now()) + 10 * 86400,
      upDownEndDate: Math.round(Date.now()) + 13 * 86400,
      maxLength: 1000,
      submissionsCount: 0,
      submissionsPerId: 1,
      submissionsPerAddress: 3,
      tokenHolder: true,
      snapshot: { 11297108109: 11597080, 11297108099: 11856756 },
      allowRichMedia: true,
      type: "mixed",
      sign: {
        _id: "642568bc7c471b504cee17ed",
        space: "63be7dd88cbe5ff1277363ae",
        label: "FOX COLLAB",
        color: "#f06c00",
        type: "proposal",
        __v: 0,
      },
      imageLink: "QmPifwoMgijVtVgTxVJmyvtW7tLve1vJ84RhCY1VphELYE",
      logoLink: "QmRZViVjNp1wsBdrCL5z1HHuMzbUj4ahr3dtqugA42C33h",
      addPermContracts: [
        "63d27ea60ca289c531ab2656",
        "63d7d2720ca289c531ab4ba6",
        "640887cc2822a419754cd177",
      ],
      votePermContracts: [
        "63d7d2720ca289c531ab4ba6",
        "63d27ea60ca289c531ab2656",
        "640887cc2822a419754cd177",
      ],
      curatorialPermContracts: [
        "63d27ea60ca289c531ab2656",
        "63d7d2720ca289c531ab4ba6",
        "640887cc2822a419754cd177",
      ],
      rules: [
        "1: But I must explain to you how all this mistaken",
        "2: The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced",
        "3: Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, to",
        "4: A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring whi",
      ],
    },
    submissions: [],
    contributors: {},
  },
  openGrant: {
    question: {
      texts: {
        callToAction:
          "A small river named Duden flows by their place and supplies it with the necessary regelialia",
        benefit: "Little Blind Text didn’t listen",
      },
      _id: "testID",
      questionHash: "testHash",
      title: "The Big Oxmox advised",
      body: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way. On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.\n\n',
      startDate: Math.round(Date.now()) - 1 * 86400,
      endDate: Math.round(Date.now()) + 10 * 86400,
      upDownEndDate: Math.round(Date.now()) + 13 * 86400,
      maxLength: 1000,
      submissionsCount: 0,
      submissionsPerId: 1,
      submissionsPerAddress: 3,
      tokenHolder: true,
      snapshot: { 11297108109: 11597080, 11297108099: 11856756 },
      allowRichMedia: true,
      type: "mixed",
      sign: {
        _id: "642568bc7c471b504cee17ed",
        space: "63be7dd88cbe5ff1277363ae",
        label: "FOX COLLAB",
        color: "#f06c00",
        type: "proposal",
        __v: 0,
      },
      imageLink: "QmPifwoMgijVtVgTxVJmyvtW7tLve1vJ84RhCY1VphELYE",
      logoLink: "QmRZViVjNp1wsBdrCL5z1HHuMzbUj4ahr3dtqugA42C33h",
      addPermContracts: [
        "63d27ea60ca289c531ab2656",
        "63d7d2720ca289c531ab4ba6",
        "640887cc2822a419754cd177",
      ],
      votePermContracts: [
        "63d7d2720ca289c531ab4ba6",
        "63d27ea60ca289c531ab2656",
        "640887cc2822a419754cd177",
      ],
      curatorialPermContracts: [
        "63d27ea60ca289c531ab2656",
        "63d7d2720ca289c531ab4ba6",
        "640887cc2822a419754cd177",
      ],
      rules: [
        "1: But I must explain to you how all this mistaken",
        "2: The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced",
        "3: Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, to",
        "4: A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring whi",
      ],
    },
    submissions: [],
    contributors: {},
  },
  openGrantWithSubmission: {
    question: {
      texts: {
        callToAction:
          "A small river named Duden flows by their place and supplies it with the necessary regelialia",
        benefit: "Little Blind Text didn’t listen",
      },
      _id: "testID",
      questionHash: "testHash",
      title: "The Big Oxmox advised",
      body: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way. On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.\n\n',
      startDate: Math.round(Date.now()) - 1 * 86400,
      endDate: Math.round(Date.now()) + 10 * 86400,
      upDownEndDate: Math.round(Date.now()) + 13 * 86400,
      maxLength: 1000,
      submissionsCount: 1,
      submissionsPerId: 1,
      submissionsPerAddress: 3,
      tokenHolder: true,
      snapshot: { 11297108109: 11597080, 11297108099: 11856756 },
      allowRichMedia: true,
      type: "mixed",
      sign: {
        _id: "642568bc7c471b504cee17ed",
        space: "63be7dd88cbe5ff1277363ae",
        label: "FOX COLLAB",
        color: "#f06c00",
        type: "proposal",
        __v: 0,
      },
      imageLink: "QmPifwoMgijVtVgTxVJmyvtW7tLve1vJ84RhCY1VphELYE",
      logoLink: "QmRZViVjNp1wsBdrCL5z1HHuMzbUj4ahr3dtqugA42C33h",
      addPermContracts: [
        "63d27ea60ca289c531ab2656",
        "63d7d2720ca289c531ab4ba6",
        "640887cc2822a419754cd177",
      ],
      votePermContracts: [
        "63d7d2720ca289c531ab4ba6",
        "63d27ea60ca289c531ab2656",
        "640887cc2822a419754cd177",
      ],
      curatorialPermContracts: [
        "63d27ea60ca289c531ab2656",
        "63d7d2720ca289c531ab4ba6",
        "640887cc2822a419754cd177",
      ],
      rules: [
        "1: But I must explain to you how all this mistaken",
        "2: The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced",
        "3: Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, to",
        "4: A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring whi",
      ],
    },
    submissions: [{
      _id: "64633489cdbab945da93af31",
      title: "First Submission",
      body: "Supports Unix timestamps in seconds, milliseconds, microseconds and nanoseconds.",
      userId: "0xd932de349A0e583b27CC98cC5025a4960C4dA5E1",
      voterAddress: "0xd932de349a0e583b27cc98cc5025a4960c4da5e1",
      timestamp: 1683292471842,
      submissionHash: "QmU9usVwPNvgNNJMtJAy84s9fxpf78JrCNHKk8pVruFGFt",
      upVotes: [],
      downVotes: [],
      richMedia: {
        _id: "64550140cdbab945da938648",
        type: "image",
        createdAt: "2023-05-05T13:14:40.566Z",
        updatedAt: "2023-05-05T13:14:40.610Z",
        __v: 0,
        link: "QmRZViVjNp1wsBdrCL5z1HHuMzbUj4ahr3dtqugA42C33h",
      },
      artist: {
        name: "John Doe",
        mediaLink: "wwwwww",
        bio: "Supports Unix timestamps in seconds, milliseconds, microseconds and nanoseconds.",
      },
      curatorialChoosen: false,
    }],
    contributors: {"0xd932de349A0e583b27CC98cC5025a4960C4dA5E1":{
      walletAddress: "0xd932de349a0e583b27cc98cc5025a4960c4da5e1",
      submissionsCounter: 1,
    }},
  },
  rankingGrant: {
    question: {
      texts: {
        callToAction:
          "A small river named Duden flows by their place and supplies it with the necessary regelialia",
        benefit: "Little Blind Text didn’t listen",
      },
      _id: "testID",
      questionHash: "testHash",
      title: "The Big Oxmox advised",
      body: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way. On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.\n\n',
      startDate: Math.round(Date.now()) - 3 * 86400,
      endDate: Math.round(Date.now()) - 1 * 86400,
      upDownEndDate: Math.round(Date.now()) + 13 * 86400,
      maxLength: 1000,
      submissionsCount: 1,
      submissionsPerId: 1,
      submissionsPerAddress: 3,
      tokenHolder: true,
      snapshot: { 11297108109: 11597080, 11297108099: 11856756 },
      allowRichMedia: true,
      type: "mixed",
      sign: {
        _id: "642568bc7c471b504cee17ed",
        space: "63be7dd88cbe5ff1277363ae",
        label: "FOX COLLAB",
        color: "#f06c00",
        type: "proposal",
        __v: 0,
      },
      imageLink: "QmPifwoMgijVtVgTxVJmyvtW7tLve1vJ84RhCY1VphELYE",
      logoLink: "QmRZViVjNp1wsBdrCL5z1HHuMzbUj4ahr3dtqugA42C33h",
      addPermContracts: [
        "63d27ea60ca289c531ab2656",
        "63d7d2720ca289c531ab4ba6",
        "640887cc2822a419754cd177",
      ],
      votePermContracts: [
        "63d7d2720ca289c531ab4ba6",
        "63d27ea60ca289c531ab2656",
        "640887cc2822a419754cd177",
      ],
      curatorialPermContracts: [
        "63d27ea60ca289c531ab2656",
        "63d7d2720ca289c531ab4ba6",
        "640887cc2822a419754cd177",
      ],
      rules: [
        "1: But I must explain to you how all this mistaken",
        "2: The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced",
        "3: Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, to",
        "4: A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring whi",
      ],
    },
    submissions: [
      {
        _id: "64633489cdbab945da93af31",
        title: "First Submission",
        body: "Supports Unix timestamps in seconds, milliseconds, microseconds and nanoseconds.",
        userId: "0xd932de349A0e583b27CC98cC5025a4960C4dA5E1",
        voterAddress: "0xd932de349a0e583b27cc98cc5025a4960c4da5e1",
        timestamp: 1683292471842,
        submissionHash: "QmU9usVwPNvgNNJMtJAy84s9fxpf78JrCNHKk8pVruFGFt",
        upVotes: [],
        downVotes: [],
        richMedia: {
          _id: "64550140cdbab945da938648",
          type: "image",
          createdAt: "2023-05-05T13:14:40.566Z",
          updatedAt: "2023-05-05T13:14:40.610Z",
          __v: 0,
          link: "QmRZViVjNp1wsBdrCL5z1HHuMzbUj4ahr3dtqugA42C33h",
        },
        artist: {
          name: "John Doe",
          mediaLink: "wwwwww",
          bio: "Supports Unix timestamps in seconds, milliseconds, microseconds and nanoseconds.",
        },
        curatorialChoosen: false,
      },
    ],
    contributors: {
      "0xd932de349A0e583b27CC98cC5025a4960C4dA5E1": {
        walletAddress: "0xd932de349a0e583b27cc98cc5025a4960c4da5e1",
        submissionsCounter: 1,
      },
    },
  },
  closedGrant: {
    question: {
      texts: {
        callToAction:
          "A small river named Duden flows by their place and supplies it with the necessary regelialia",
        benefit: "Little Blind Text didn’t listen",
      },
      _id: "testID",
      questionHash: "QmZebTPWBJS9wA986ivNNyD7yG1t35vVHG1ZTZpNrEcUfe",
      title: "The Big Oxmox advised",
      body: 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way. On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.\n\n',
      startDate: Math.round(Date.now()) - 3 * 86400,
      endDate: Math.round(Date.now()) - 2 * 86400,
      upDownEndDate: Math.round(Date.now()) - 1 * 86400,
      maxLength: 1000,
      submissionsCount: 1,
      submissionsPerId: 1,
      submissionsPerAddress: 3,
      tokenHolder: true,
      snapshot: { 11297108109: 11597080, 11297108099: 11856756 },
      allowRichMedia: true,
      type: "mixed",
      sign: {
        _id: "642568bc7c471b504cee17ed",
        space: "63be7dd88cbe5ff1277363ae",
        label: "FOX COLLAB",
        color: "#f06c00",
        type: "proposal",
        __v: 0,
      },
      imageLink: "QmPifwoMgijVtVgTxVJmyvtW7tLve1vJ84RhCY1VphELYE",
      logoLink: "QmRZViVjNp1wsBdrCL5z1HHuMzbUj4ahr3dtqugA42C33h",
      addPermContracts: [
        "63d27ea60ca289c531ab2656",
        "63d7d2720ca289c531ab4ba6",
        "640887cc2822a419754cd177",
      ],
      votePermContracts: [
        "63d7d2720ca289c531ab4ba6",
        "63d27ea60ca289c531ab2656",
        "640887cc2822a419754cd177",
      ],
      curatorialPermContracts: [
        "63d27ea60ca289c531ab2656",
        "63d7d2720ca289c531ab4ba6",
        "640887cc2822a419754cd177",
      ],
      rules: [
        "1: But I must explain to you how all this mistaken",
        "2: The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced",
        "3: Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, to",
        "4: A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring whi",
      ],
    },
    submissions: [
      {
        _id: "64633489cdbab945da93af31",
        title: "First Submission",
        body: "Supports Unix timestamps in seconds, milliseconds, microseconds and nanoseconds.",
        userId: "0xd932de349A0e583b27CC98cC5025a4960C4dA5E1",
        voterAddress: "0xd932de349a0e583b27cc98cc5025a4960c4da5e1",
        timestamp: 1683292471842,
        submissionHash: "QmU9usVwPNvgNNJMtJAy84s9fxpf78JrCNHKk8pVruFGFt",
        upVotes: [],
        downVotes: [],
        richMedia: {
          _id: "64550140cdbab945da938648",
          type: "image",
          createdAt: "2023-05-05T13:14:40.566Z",
          updatedAt: "2023-05-05T13:14:40.610Z",
          __v: 0,
          link: "QmRZViVjNp1wsBdrCL5z1HHuMzbUj4ahr3dtqugA42C33h",
        },
        artist: {
          name: "John Doe",
          mediaLink: "wwwwww",
          bio: "Supports Unix timestamps in seconds, milliseconds, microseconds and nanoseconds.",
        },
        curatorialChoosen: false,
      },
    ],
    contributors: {
      "0xd932de349A0e583b27CC98cC5025a4960C4dA5E1": {
        walletAddress: "0xd932de349a0e583b27cc98cc5025a4960c4da5e1",
        submissionsCounter: 1,
      },
    },
  },
};

function generateJSONfiles() {
  const grantNames = Object.keys(mockGrants);
  for (let grantName of grantNames) {
    writeJSONToFile(grantName);
  }
}

function writeJSONToFile(fileName) {
  const JSfilePath = path.join('./cypress/fixtures', `${fileName}.json`);
  fs.writeFile(JSfilePath, JSON.stringify(mockGrants[fileName]), function(){console.log(fileName + " saved")});
}

generateJSONfiles()
