const { defineConfig } = require("cypress");
const helpers = require("./cypress/helpers");
const puppeteer = require("./cypress/support/puppeteer");
const metamask = require("./cypress/support/metamask");
const SECRET_WORDS = [
  "word1",
  "word2",
  "word3",
  "word4",
  "word5",
  "word6",
  "word7",
  "word8",
  "word9",
  "word10",
  "word11",
  "word12",
];

/**
 * @type {Cypress.PluginConfig}
 */

module.exports = defineConfig({
  viewportHeight: 1080,
  viewportWidth: 1920,
  chromeWebSecurity: true,
  defaultCommandTimeout: 30000,
  pageLoadTimeout: 40000,
  requestTimeout: 30000,
  video: false,
  
  e2e: {
    setupNodeEvents(on, config) {
      // `on` is used to hook into various events Cypress emits
      // `config` is the resolved Cypress config
      on("before:browser:launch", async (browser, arguments_) => {
        if (browser.family === "chromium" && browser.isHeadless) {
          console.log("TRUE"); // required by cypress ¯\_(ツ)_/¯

          arguments_.args.push(
            "--window-size=1920,1080",
            "--disable-background-timer-throttling",
            "--disable-backgrounding-occluded-windows",
            "--disable-renderer-backgrounding"
          );
          return arguments_;
        }

        // // metamask welcome screen blocks cypress from loading
        // if (browser.name === "chrome") {
        //   arguments_.args.push(
        //     "--disable-background-timer-throttling",
        //     "--disable-backgrounding-occluded-windows",
        //     "--disable-renderer-backgrounding"
        //   );
        // }
        if (!process.env.SKIP_METAMASK_INSTALL) {
          // NOTE: extensions cannot be loaded in headless Chrome
          const metamaskPath = await helpers.prepareMetamask("10.24.0");
          // process.env.METAMASK_VERSION || "10.24.0"
          arguments_.extensions.push(metamaskPath);
        }
        return arguments_;
      });

      on("task", {
        error(message) {
          console.error("\u001B[31m", "ERROR:", message, "\u001B[0m");
          return true;
        },
        warn(message) {
          console.warn("\u001B[33m", "WARNING:", message, "\u001B[0m");
          return true;
        },
        // puppeteer commands
        initPuppeteer: async () => {
          const connected = await puppeteer.init();
          return connected;
        },
        clearPuppeteer: async () => {
          const cleared = await puppeteer.clear();
          return cleared;
        },
        assignWindows: async () => {
          const assigned = await puppeteer.assignWindows();
          return assigned;
        },
        clearWindows: async () => {
          const cleared = await puppeteer.clearWindows();
          return cleared;
        },
        assignActiveTabName: async (tabName) => {
          const assigned = await puppeteer.assignActiveTabName(tabName);
          return assigned;
        },
        isMetamaskWindowActive: async () => {
          const isMetamaskActive = await puppeteer.isMetamaskWindowActive();
          return isMetamaskActive;
        },
        isCypressWindowActive: async () => {
          const isCypressActive = await puppeteer.isCypressWindowActive();
          return isCypressActive;
        },
        switchToCypressWindow: async () => {
          const switched = await puppeteer.switchToCypressWindow();
          return switched;
        },
        switchToMetamaskWindow: async () => {
          const switched = await puppeteer.switchToMetamaskWindow();
          return switched;
        },
        switchToMetamaskNotification: async () => {
          const notificationPage =
            await puppeteer.switchToMetamaskNotification();
          return notificationPage;
        },
        unlockMetamask: async (password) => {
          const unlocked = await metamask.unlock(password);
          return unlocked;
        },
        importMetamaskAccount: async (privateKey) => {
          const imported = await metamask.importAccount(privateKey);
          return imported;
        },
        createMetamaskAccount: async (accountName) => {
          const created = await metamask.createAccount(accountName);
          return created;
        },
        switchMetamaskAccount: async (accountNameOrAccountNumber) => {
          const switched = await metamask.switchAccount(
            accountNameOrAccountNumber
          );
          return switched;
        },
        addMetamaskNetwork: async (network) => {
          const networkAdded = await metamask.addNetwork(network);
          return networkAdded;
        },
        changeMetamaskNetwork: async (network) => {
          if (process.env.NETWORK_NAME && !network) {
            network = process.env.NETWORK_NAME;
          } else if (!network) {
            network = "mainnet";
          }
          const networkChanged = await metamask.changeNetwork(network);
          return networkChanged;
        },
        activateCustomNonceInMetamask: async () => {
          const activated = await metamask.activateCustomNonce();
          return activated;
        },
        resetMetamaskAccount: async () => {
          const resetted = await metamask.resetAccount();
          return resetted;
        },
        disconnectMetamaskWalletFromDapp: async () => {
          const disconnected = await metamask.disconnectWalletFromDapp();
          return disconnected;
        },
        disconnectMetamaskWalletFromAllDapps: async () => {
          const disconnected = await metamask.disconnectWalletFromAllDapps();
          return disconnected;
        },
        confirmMetamaskSignatureRequest: async () => {
          const confirmed = await metamask.confirmSignatureRequest();
          return confirmed;
        },
        rejectMetamaskSignatureRequest: async () => {
          const rejected = await metamask.rejectSignatureRequest();
          return rejected;
        },
        confirmMetamaskPermissionToSpend: async () => {
          const confirmed = await metamask.confirmPermissionToSpend();
          return confirmed;
        },
        rejectMetamaskPermissionToSpend: async () => {
          const rejected = await metamask.rejectPermissionToSpend();
          return rejected;
        },
        acceptMetamaskAccess: async (allAccounts) => {
          const accepted = await metamask.acceptAccess(allAccounts);
          return accepted;
        },
        confirmMetamaskTransaction: async (gasConfig) => {
          const confirmed = await metamask.confirmTransaction(gasConfig);
          return confirmed;
        },
        rejectMetamaskTransaction: async () => {
          const rejected = await metamask.rejectTransaction();
          return rejected;
        },
        allowMetamaskToAddNetwork: async () => {
          const allowed = await metamask.allowToAddNetwork();
          return allowed;
        },
        rejectMetamaskToAddNetwork: async () => {
          const rejected = await metamask.rejectToAddNetwork();
          return rejected;
        },
        allowMetamaskToSwitchNetwork: async () => {
          const allowed = await metamask.allowToSwitchNetwork();
          return allowed;
        },
        rejectMetamaskToSwitchNetwork: async () => {
          const rejected = await metamask.rejectToSwitchNetwork();
          return rejected;
        },
        allowMetamaskToAddAndSwitchNetwork: async () => {
          const allowed = await metamask.allowToAddAndSwitchNetwork();
          return allowed;
        },
        getMetamaskWalletAddress: async () => {
          const walletAddress = await metamask.getWalletAddress();
          return walletAddress;
        },
        fetchMetamaskWalletAddress: async () => {
          return metamask.walletAddress();
        },
        setupMetamask: async ({
          secretWordsOrPrivateKey,
          network = "mainnet",
          password,
        }) => {
          if (process.env.NETWORK_NAME) {
            network = process.env.NETWORK_NAME;
          }
          if (process.env.PRIVATE_KEY) {
            secretWordsOrPrivateKey = process.env.PRIVATE_KEY;
          }
          if (SECRET_WORDS) {
            secretWordsOrPrivateKey = SECRET_WORDS;
          }
          await metamask.initialSetup({
            secretWordsOrPrivateKey,
            network,
            password,
          });
          return true;
        },
        getNetwork: () => {
          const network = helpers.getNetwork();
          return network;
        },
      });

      if (process.env.BASE_URL) {
        config.baseUrl = process.env.BASE_URL;
      }

      if (process.env.CI) {
        config.retries.runMode = 1;
        config.retries.openMode = 1;
      }

      if (process.env.SKIP_METAMASK_SETUP) {
        config.env.SKIP_METAMASK_SETUP = true;
      }

      // next component testing
      if (config.testingType === "component") {
        require("@cypress/react/plugins/next")(on, config);
      }

      return config;
    },

    baseUrl: "https://palm.factorydao.neti-soft.com/",
    supportFile:'./cypress/support/e2e.js',
  },
});
