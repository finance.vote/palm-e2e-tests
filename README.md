# Palm-e2e-tests
## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/finance.vote/palm-e2e-tests.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/finance.vote/palm-e2e-tests/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
## Description
This project is an automated test script written with cypress to test features of palm.factorydao.neti-soft.com with usage of puppeteer to allow test the palm app with metamask extension. 

## Before run
In the main folder, add file `cypress.env.json` and put an object with metamask seed phrase inside (metamask seed phrase contains 12 words):

    {
        SECRET_WORDS = [
            "word1",
            "word2",
            "word3"
        ]
    }

## Installation & run

    npm i
    npm run cypr

## Support
    
    mp@neti-soft.com

## Project structure

    ├── cypress
    │    ├── downloads              # chrome extension download destination
    │    ├── e2e                    # automatic test scripts
    │    ├── fixtures               # external pieces of static data that are used by tests
    │    ├── pages                  # files with selectors used within extension's and project's pages
    │    │   ├── metamask           # extension's pages selectors
    │    │   └── palm               # project's pages selectors
    │    ├── support                # code files included before test files, fired by cypress.config.js file
    │    └── helpers.js             # helper file to install metamask extension on cypress open
    ├── cypress.config.js           
    └── README.md

